<?php

namespace SnowFlake\Util;

class MathUtil
{
    /**
     * @param int $leftMoveLength
     * @return int
     */
    public static function getLimitBitMaxNumber(int $leftMoveLength): int
    {
        return ((1 << $leftMoveLength) - 1);
    }

    /**
     * @param int $number
     * @param int $bitLength
     * @return int
     */
    public static function getBitMod(int $number, int $bitLength): int
    {
        return abs($number) & static::getLimitBitMaxNumber($bitLength);
    }

}