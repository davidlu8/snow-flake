<?php
declare(strict_types=1);

namespace SnowFlake\Engine;

interface SnowFlakeEngineInterface
{
    /**
     * @param array $config
     */
    public function setConfig(array $config): void;

    /**
     * @param string $keyString
     * @return mixed
     */
    public function getConfig(string $keyString);

    /**
     * @param array $params
     */
    public function setParams(array $params): void;

    /**
     * @return int
     */
    public function id(): int;

    /**
     * @param int $id
     * @return bool
     */
    public function verify(int $id): bool;
}