<?php

declare(strict_types=1);

namespace SnowFlake\Tests\Engine;

use Contract\Exceptions\ValidationException;
use SnowFlake\Engine\SnowFlakeEngine;
use SnowFlake\Tests\TestCase;

class SnowFlakeEngineTest extends TestCase
{
    /** @var SnowFlakeEngine $snowFlakeEngine */
    protected SnowFlakeEngine $snowFlakeEngine;

    public function setUp()
    {
        $this->snowFlakeEngine = new SnowFlakeEngine();
    }

    public function dpGetConfig(): array
    {
        return [
            [
                [
                    'fields' => [],
                    'extends' => [
                        'verify_code_algorithm' => [
                            'seed' => 'hakuna_matata',
                            'factors' => [8, 218]
                        ]
                    ],
                ],
                'extends.verify_code_algorithm.seed',
                'hakuna_matata'
            ],
            [
                [
                    'fields' => [],
                    'extends' => [
                        'verify_code_algorithm' => [
                            'seed' => 'hakuna_matata',
                            'factors' => [8, 218]
                        ]
                    ],
                ],
                'extends.verify_code_algorithm.factors',
                [8, 218]
            ],
            [
                [
                    'fields' => [],
                    'extends' => [
                        'verify_code_algorithm' => [
                            'seed' => 'hakuna_matata',
                            'factors' => [8, 218]
                        ]
                    ],
                ],
                'extends.verify_code_algorithm.factors.1',
                218
            ]
        ];
    }

    /**
     * @dataProvider dpGetConfig
     * @param array $config
     * @param string $key
     * @param mixed $expect
     * @return void
     * @throws ValidationException
     */
    public function testGetConfig(array $config, string $key, $expect): void
    {
        $this->snowFlakeEngine->setConfig($config);
        $this->assertEquals($expect, $this->snowFlakeEngine->getConfig($key));
    }

    public function dpId(): array
    {
        return [
            [
                10001, 27249870990,
            ],
            [
                10002, 27249870995,
            ],
            [
                10003, 27249871005,
            ],
            [
                10004, 27249871012,
            ],
            [
                10005, 27249871017,
            ]
        ];
    }

    /**
     * @dataProvider dpId
     * @param int $sequence
     * @param int $id
     * @throws ValidationException
     */
    public function testId(int $sequence, int $id)
    {
        $this->snowFlakeEngine->setConfig([
            'fields' => [
                'timestamp' => [
                    'length' => 28,
                ],
                'sequence' => [
                    'length' => 9,
                ],
                'verify_code' => [
                    'length' => 3,
                ],
            ]
        ]);
        $this->snowFlakeEngine->setParams([
            'timestamp' => strtotime('2022-03-19 00:00:00') - strtotime('2022-01-01 00:00:00'),
            'sequence' => $sequence,
        ]);
        $this->assertEquals($id, $this->snowFlakeEngine->id());
    }

    public function dpVerify(): array
    {
        return [
            [true, 27249870990],
            [false, 27249870991],
        ];
    }

    /**
     * @dataProvider dpVerify
     * @param $expect
     * @param $id
     * @throws ValidationException
     */
    public function testVerify($expect, $id)
    {
        $this->snowFlakeEngine->setConfig([
            'fields' => [
                'timestamp' => [
                    'length' => 28,
                ],
                'sequence' => [
                    'length' => 9,
                ],
                'verify_code' => [
                    'length' => 3,
                ]
            ]
        ]);
        $this->assertEquals($expect, $this->snowFlakeEngine->verify($id));
    }

    public function dpId1(): array
    {
        return [
            [
                1, '2022-06-07 00:00:00', 10001, 36052519288849,
            ],
            [
                2, '2022-06-07 00:00:00', 10002, 71236891377682,
            ],
            [
                3, '2022-06-07 00:00:00', 10003, 106421263466515,
            ],
            [
                4, '2022-06-07 00:00:00', 10004, 141605635555348,
            ],
            [
                5, '2022-06-07 00:00:00', 10005, 176790007644181,
            ]
        ];
    }

    /**
     * @dataProvider dpId1
     * @param int $scene
     * @param string $currentDateTime
     * @param int $sequence
     * @param int $id
     * @throws ValidationException
     */
    public function testId1(int $scene, string $currentDateTime, int $sequence, int $id)
    {
        $this->snowFlakeEngine->setConfig([
            'fields' => [
                'scene' => [
                    'length' => 8
                ],
                'timestamp' => [
                    'length' => 39,
                ],
                'sequence' => [
                    'length' => 6,
                ],
            ]
        ]);
        $this->snowFlakeEngine->setParams([
            'scene' => $scene,
            'timestamp' => strtotime($currentDateTime) * 1000 - strtotime('2022-01-01 00:00:00') * 1000,
            'sequence' => $sequence,
        ]);
        $this->assertEquals($id, $this->snowFlakeEngine->id());
    }
}