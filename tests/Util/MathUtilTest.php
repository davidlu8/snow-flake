<?php

declare(strict_types=1);

namespace SnowFlake\Tests\Util;

use SnowFlake\Tests\TestCase;
use SnowFlake\Util\MathUtil;

class MathUtilTest extends TestCase
{
    public function dpGetLimitBitMaxNumber(): array
    {
        return [
            [255, 8],
            [268435455, 28],
        ];
    }

    /**
     * @dataProvider dpGetLimitBitMaxNumber
     * @param $number
     * @param $bitLength
     */
    public function testGetLimitBitMaxNumber($number, $bitLength): void
    {
        $this->assertEquals($number, MathUtil::getLimitBitMaxNumber($bitLength));
    }

    public function dpGetBitMod(): array
    {
        return [
            [255, 8, 255],
            [256, 8, 0],
            [260, 8, 4],
            [513, 8, 1],
            [1030, 8, 6],
        ];
    }

    /**
     * @dataProvider dpGetBitMod
     * @param $number
     * @param $bitLength
     * @param $remainder
     */
    public function testGetBitMod($number, $bitLength, $remainder): void
    {
        $this->assertEquals($remainder, MathUtil::getBitMod($number, $bitLength));
    }

    public function testDD()
    {
        $this->assertEquals(1, (int)('0.01' * 100));
    }
}